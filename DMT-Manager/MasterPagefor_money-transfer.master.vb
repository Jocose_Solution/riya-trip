﻿Partial Class MasterPagefor_money_transfer
    Inherits System.Web.UI.MasterPage
    Protected Sub Logout_Click(sender As Object, e As EventArgs)
        Try
            FormsAuthentication.SignOut()
            Session.Abandon()
            Response.Redirect("~/Login.aspx")
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
End Class

