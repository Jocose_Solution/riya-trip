﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="SenderIndex.aspx.cs" Inherits="DMT_Manager_dmt_SenderIndex" %>

<%@ Register Src="~/DMT-Manager/User_Control/uc_dmt_model.ascx" TagPrefix="uc1" TagName="uc_dmt_model" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />

    <section class="hero-wrap section shadow-md py-4">
        <div class="hero-mask opacity-7 bg-dark"></div>
        <div class="hero-bg" style="background-image: url('images/bg/image-6.jpg');"></div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-xl-10 my-auto" style="margin-left: auto; margin-right: auto;">
                        <div class="bg-white rounded shadow-md p-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3 class="text-5 text-center">Enter Sender’s Mobile Number</h3>
                                </div>
                                <%--<div class="col-sm-9">
                                    <h3 class="text-5 text-center">Enter Sender’s Mobile Number</h3>
                                </div>
                                <div class="col-sm-3"><span id="btnNewRegistration" style="cursor: pointer;" class="btn btn-sm btn-primary btn-block" onclick="NewRegistration();">New Registration</span></div>--%>
                            </div>
                            <hr class="mb-4" />
                            <div class="row">
                                <div class="col-sm-9 col-lg-offcet-3" style="margin-left: auto; margin: auto;">
                                    <div class="row">
                                        <div class="col-lg-8 form-validation">
                                            <input type="text" id="txtSenderMobileNo" value="" class="form-control sendmobileno" placeholder="Sender Mobile No." maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                                            <p style="color: #ccc;">(10 digits) Please don not use prefix zero (0)</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <span id="btnRemitterMobileSearch" style="cursor: pointer;" class="btn btn-primary btn-block" onclick="RemitterMobileSearch();">Search</span>
                                            <p class="text-danger" id="TokenExpired"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 my-auto" id="RemitterDetailsByMobile"></div>
                </div>
            </div>
        </div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 1</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Register a Sender</h3>
                                <p>Fill in basic information and register a Sender for Money Transfer. Upgrade Sender to KYC by uploading a Photo Id and an Address Proof.</p>
                                <%-- <p>मूलभूत जानकारी भरकर प्रेषक को पैसा भेजने के लिए पंजीकृत करें | एक फोटो आई डी और पता प्रमाण अपलोड कर प्रेषक की जानकारीअपग्रेड करें।</p>--%>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 2</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Add a Beneficiary</h3>
                                <p>Add multiple Beneficiaries / Receivers against each Sender.</p>
                                <%--<p>प्रत्येक प्रेषक के अन्तर्गत विभिन्न लाभार्थीं / प्राप्तकर्ता जोड़ सकते हैं।</p>--%>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 3</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Transfer Money</h3>
                                <p>Instantly begin transferring money.</p>
                                <%--<p>तुरंत पैसा भेजने की प्रक्रिया शुरू करें।</p>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <%--<button type="button" class="btn btn-primary VerifyTransferDetails" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#VerifyTransferDetails">Hello Dear</button>
    <div class="modal fade" id="VerifyTransferDetails">
        <div class="modal-dialog modal-lg" style="margin: 6% auto!important;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transveryheading">Transaction Summary</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #ff414d;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="border-bottom: 1px solid #dee2e6; padding: 0.7rem;">
                        <div class="col-sm-4">
                            <p>From</p>
                            <span>Krishna (9555266308)</span>
                            <span>delhi uttam nagar 110059</span>
                        </div>
                        <div class="col-sm-4" style="text-align: center;">
                            <p class="text-center">
                                <span class="fa fa-check-circle text-center text-success" style="font-size: 70px;"></span>
                                <br />
                                <span class="text-center" style="font-size: 20px;">Track ID</span><br />
                                <span class="text-center" style="font-size: 15px;color:#ff414d;">D433DD9EABDEA2CBA57B</span>
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <p>To</p>
                            <span>H S Gandhi</span>
                            <span>HDFC - (hgfjhjkh)</span>
                            <span>965456656565464</span>
                        </div>
                    </div>
                    <div class="row" style="padding: 1rem;">
                        <div class="col-sm-12">
                            <div class="row" style="border-bottom: 1px dotted #ccc;">
                                <div class="col-sm-3" style="margin-bottom: 10px;"><b>ORDER ID</b></div>
                                <div class="col-sm-3" style="margin-bottom: 10px;"><b>ORDER VALUE</b></div>
                                <div class="col-sm-2" style="margin-bottom: 10px;"><b>DR/CR AMT</b></div>
                                <div class="col-sm-2" style="margin-bottom: 10px;"><b>BALANCE</b></div>
                                <div class="col-sm-2" style="margin-bottom: 10px;"><b>STATUS</b></div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding: 15px;">
                            <div class="row" style="margin-bottom: 10px; border-bottom: 1px dotted #ccc;">
                                <div class="col-sm-3" style="margin-bottom: 10px;">1200818131003NBVXA<br /><span style="color:#b0b0b0;">18 Aug 2020 01:10 PM</span></div>
                                <div class="col-sm-3" style="margin-bottom: 10px;">₹ 11.00</div>
                                <div class="col-sm-2" style="margin-bottom: 10px;">₹ 17.66</div>
                                <div class="col-sm-2" style="margin-bottom: 10px;">₹ 1,572.89</div>
                                <div class="col-sm-2 text-success" style="margin-bottom: 10px;">SUCCESS</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px; border-bottom: 1px dotted #ccc;">
                                <div class="col-sm-3" style="margin-bottom: 10px;">1200818131003NBVXA</div>
                                <div class="col-sm-3" style="margin-bottom: 10px;">₹ 11.00</div>
                                <div class="col-sm-2" style="margin-bottom: 10px;">₹ 17.66</div>
                                <div class="col-sm-2" style="margin-bottom: 10px;">₹ 1,572.89</div>
                                <div class="col-sm-2 text-danger" style="margin-bottom: 10px;">SUCCESS</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px; border-bottom: 1px dotted #ccc;">
                                <div class="col-sm-3" style="margin-bottom: 10px;">1200818131003NBVXA</div>
                                <div class="col-sm-3" style="margin-bottom: 10px;">₹ 11.00</div>
                                <div class="col-sm-2" style="margin-bottom: 10px;">₹ 17.66</div>
                                <div class="col-sm-2" style="margin-bottom: 10px;">₹ 1,572.89</div>
                                <div class="col-sm-2 text-warning" style="margin-bottom: 10px;">SUCCESS</div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>--%>

    <uc1:uc_dmt_model runat="server" ID="uc_dmt_model" />

    <script src="custom/js/new_dmt.js"></script>
    <script src="custom/js/common.js"></script>
</asp:Content>

