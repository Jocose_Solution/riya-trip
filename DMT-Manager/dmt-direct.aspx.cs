﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;

public partial class DMT_Manager_dmt_direct : System.Web.UI.Page
{
    private static string UserId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();
        }
        else
        {
            Response.Redirect("/");
        }
    }

    #region [Json Section]

    [WebMethod]
    public static List<string> DMTDRemitterMobileSearch(string mobileno)
    {
        List<string> result = new List<string>();
        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(mobileno))
                {
                    DataTable dtRemitter = InstantPay_ApiService.GetDMTDirectRemitter(mobileno, UserId);

                    if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                    {
                        bool isverified = dtRemitter.Rows[0]["IsVerified"].ToString().ToLower() == "false" ? false : true;
                        string remitterid = dtRemitter.Rows[0]["RemitterId"].ToString();

                        if (isverified)
                        {
                            result.Add("success");
                            result.Add("/dmt-manager/dmt-direct-fund-transfer.aspx?mobile=" + mobileno + "&sender=" + remitterid);
                        }
                        else
                        {
                            bool isOtpSend = InstantPay_ApiService.ResendDMTDOtp(UserId, mobileno, remitterid);

                            if (isOtpSend)
                            {
                                result.Add("otpsent");
                                result.Add(mobileno);
                                result.Add(remitterid);
                            }
                            else
                            {
                                result.Add("registration");
                            }
                        }
                    }
                    else
                    {
                        result.Add("registration");
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DMTDRemitterRegistration(string mobile, string firstname, string lastname, string pincode, string localadd)
    {
        List<string> result = new List<string>();
        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(pincode) && !string.IsNullOrEmpty(localadd))
                {
                    string remitterid = "DMTD" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 7).ToUpper();

                    string otp = string.Empty;
                    bool isSuccess = InstantPay_ApiService.InsertDMTDirectRemitter(mobile, firstname, lastname, pincode, localadd, UserId, remitterid, ref otp);

                    if (isSuccess)
                    {
                        result.Add("success");
                        result.Add(remitterid);
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add("Registration failed, Please try again!");
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DMTDRemitterVarification(string mobile, string remitterid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(remitterid) && !string.IsNullOrEmpty(otp))
                {
                    DataTable dtRemitter = InstantPay_ApiService.GetDMTDirectRemitter(mobile, UserId, remitterid);

                    if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                    {
                        string dbotp = dtRemitter.Rows[0]["OTP"].ToString();
                        string dbotpexp = dtRemitter.Rows[0]["OTPExpTime"].ToString();

                        DateTime dtExpOtp = new DateTime();
                        dtExpOtp = Convert.ToDateTime(dbotpexp);

                        TimeSpan ts = DateTime.Now - dtExpOtp;

                        if (ts.Minutes < 3)
                        {
                            if (dbotp == otp)
                            {
                                if (InstantPay_ApiService.VerifyingDMTDRemitter(UserId, mobile, remitterid))
                                {
                                    result.Add("success");
                                    result.Add("/dmt-manager/dmt-direct-fund-transfer.aspx?mobile=" + mobile + "&sender=" + remitterid);
                                }
                            }
                            else
                            {
                                result.Add("failed");
                                result.Add("Wrong OTP");
                            }
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("OTP expired, please resend otp.");
                        }
                    }
                    else
                    {
                        result.Add("registration");
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DMTDResendOtpToRemitterMobile(string mobileno, string remitterid)
    {
        List<string> result = new List<string>();

        try
        {
            try
            {
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (!string.IsNullOrEmpty(mobileno) && !string.IsNullOrEmpty(remitterid))
                    {
                        bool isOtpSend = InstantPay_ApiService.ResendDMTDOtp(UserId, mobileno, remitterid);

                        if (isOtpSend)
                        {
                            result.Add("otpsent");
                        }
                        else
                        {
                            result.Add("error");
                            result.Add("Error occured during otp sending.");
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                result.Add(ex.Message);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    #endregion
}