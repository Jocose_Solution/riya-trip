﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb"
    Inherits="UserControl_LoginControl" %>








<asp:Login ID="UserLogin" runat="server">


    <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>

        <style>
            .featured-box2 {
                margin-left: auto;
                margin-right: auto;
                position: relative;
                border: 1px solid #ccc;
                text-align: center;
                padding: 6px;
                /*box-shadow: 0px 0px 4px #ccc;
                border-radius: 5px;*/
            }

                .featured-box2:hover {
                    border: 1px solid #ebeded;
                    -webkit-box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                    box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                }

            table {
                border-collapse: collapse;
                width: 100% !important;
            }

            .textboxcss {
                padding: 9px 10px !important;
                width: 100% !important;
                margin-bottom: 10px !important;
            }

            .dcv {
                min-height: 500px;
                background-image: url(../../DMT-Manager/images/bg/BackgroundImage.jpg);
            }

            .contenthed {
                border: 1px solid #0d6d86;
                padding: 60px;
                font-size: 59px;
                margin-top: 80px;
                margin-left: 20px;
                border-radius: 4px;
                color: #13688f;
            }

            .featured-box {
                box-sizing: border-box;
                margin-left: auto;
                margin-right: auto;
                padding: 10px;
                position: relative;
                border: 1px solid #ccc;
                text-align: center;
            }

                .featured-box:hover {
                    border: 1px solid #ebeded;
                    -webkit-box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                    box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                }

            .formbox {
                width: 75%;
                background: #fff;
                padding: 30px;
                margin-top: 50px;
                border-radius: 4px;
                color: #ff414d;
                margin-left: 62px;
                box-shadow: 0 0 25px rgba(0,0,0,.3);
            }

            .btn:hover {
                color: #ffffff !important;
                background: #106987 !important;
                text-decoration: none;
            }

            .btnwidth {
                background: #106987 !important;
                width: 100% !important;
                font-size: 18px !important;
            }

            .bg-primary, .badge-primary {
                background-color: #526681 !important;
            }

            .profileimage {
                width: 170px;
                height: 170px;
                border: 1px solid #ccc;
                padding: 5px;
                background: #ffffff;
                border-radius: 100px;
                margin-top: -10px;
                margin-left: 80px;
            }
        </style>

        <div id="content">

            <div class="dcv">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">                           
                           <div class="col-sm-12 formbox">
                                <h4 class="text-center">Welcome to Riya Trip B2B</h4>
                            <p class="text-center"><span class="fa fa-sign-in"></span>&nbsp;
                                Sign in to continue
                            </p>
                               <div class="col-sm-12">
                                    <h6 class="mb-0 text-sm">User Id</h6>
                                     <asp:TextBox runat="server" class="mb-4 form-control textboxcss" ID="UserName" placeholder="Enter a valid User Id"></asp:TextBox>
                               </div>
                                 <div class="col-sm-12">
                                     <h6 class="mb-0 text-sm">Password</h6>
                                        <asp:TextBox ID="Password"   TextMode="Password" placeholder="Enter password" Class="form-control textboxcss" runat="server"></asp:TextBox>                            
                                 </div>                               
                                <div class="col-sm-12 ">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="custom-control custom-checkbox custom-control-inline"> <input id="chk1" type="checkbox" name="chk" class="custom-control-input"> <label for="chk1" class="custom-control-label text-sm">Remember me</label> </div>
                                        </div>
                                        <div class="col-sm-6">
                                               <a href="../ForgotPassword.aspx" class="ml-auto mb-0 text-sm">Forgot Password?</a>
                                        </div>
                                    </div>
                                          
                                       </div>
                               <div class="col-sm-12  form-group">                                   
                                   <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-blue text-center btnwidth"/>
                               </div>
                               <div class="col-sm-12">                                  
                                            <small class="font-weight-bold" style="font-size:14px !important;">Don't have an account? <a href="../regs_new.aspx" class="text-danger ">Register</a></small>                                      
                               </div>                         
                        
                    </div>                             
                        </div>  
                        <div class="col-sm-6 ">
                            <h1 class="contenthed">India's leading B2B travel portal
                            </h1>
                        </div>
                    </div>
                </div>
            </div>


            <section class="section bg-light" style="padding: 10px; margin-bottom:40px;">
                <div class="container">
                    <h2 class="text-9 text-center">Our Services</h2>                   
                    <div class="row">
                        <div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
                            <div class="featured-box2">
                                <div class="featured-box-icon text-primary"><i class="fas fa-plane-departure" style="font-size: 50px;"></i></div>
                                <h3 style="font-size: 20px;">Flight Booking</h3>
                                <p class="text-3">Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                              
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
                            <div class="featured-box2">
                                <div class="featured-box-icon text-primary"><i class="fas fa-share-square"  style="font-size: 50px;"></i></div>
                                <h3 style="font-size: 20px;">Money Transfer</h3>
                                <p class="text-3">Persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                               
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
                            <div class="featured-box2">
                                <div class="featured-box-icon text-primary"><i class="fas fa-mobile" style="font-size: 50px;" ></i></div>
                                <h3 style="font-size: 20px;">Mobile Recharge</h3>
                                <p class="text-3">Essent lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="featured-box2">
                                <div class="featured-box-icon text-primary"><i class="fas fa-hand-holding-usd" style="font-size: 50px;"></i></div>
                                <h3 style="font-size: 20px;">Bill Payments</h3>
                                <p class="text-3">Quidam lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
         <section class="hero-wrap section shadow-md" style="margin-bottom:40px;height: 300px !important;">
                <div class="hero-mask opacity-9 bg-primary"></div>
                <div class="hero-bg" style="background-image: url('DMT-Manager/images/bg/image-2.jpg');"></div>             
                <div class="hero-content py-5" style="margin-top:-100px">
                     <h2 class="text-9 text-white text-center" style="font-size: 25px !important;">Message from Founder & Managing Director</h2>
                         <div class="row" style="width:100%">
                            
                             <div class="col-sm-3">
                        <img src="DMT-Manager/images/bg/dirctor.png" class="profileimage" />                      
                    </div>
                    <div class="col-sm-9">
                        <p class="text-4 text-white mb-4" style="font-size: 16px !important;padding: 20px">As I take stock of what has inspired over the years, I am extremely proud of our achievements since 1992. The future looks even more promising, with plans for growth fully underway. We have successfully transitioned from a local Bangalore based one branch operation to become a respected and reputed company garnering business all across the nation with earning our Clients Trust and Respect.</p>
                    </div> 
                         </div>              
                </div>
            </section>
            <section class="section bg-light" style="padding: 15px;margin-bottom:40px;">
                <div class="container">
                    <h2 class="text-9 text-center">Why Choose Us</h2>                   
                    <div class="row">
                        <div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
                            <div class="featured-box">
                                <div class="featured-box-icon text-primary"><i class="fas fa-hand-pointer"></i></div>
                                <h3>Easy to use</h3>
                                <p class="text-3">Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                                <a href="#" class="btn-link text-3">Learn more<i class="fas fa-chevron-right text-1 ml-2"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
                            <div class="featured-box">
                                <div class="featured-box-icon text-primary"><i class="fas fa-share"></i></div>
                                <h3>Faster Payments</h3>
                                <p class="text-3">Persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                                <a href="#" class="btn-link text-3">Learn more<i class="fas fa-chevron-right text-1 ml-2"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
                            <div class="featured-box">
                                <div class="featured-box-icon text-primary"><i class="fas fa-dollar-sign"></i></div>
                                <h3>Lower Fees</h3>
                                <p class="text-3">Essent lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                                <a href="#" class="btn-link text-3">Learn more<i class="fas fa-chevron-right text-1 ml-2"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="featured-box">
                                <div class="featured-box-icon text-primary"><i class="fas fa-lock"></i></div>
                                <h3>100% secure</h3>
                                <p class="text-3">Quidam lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                                <a href="#" class="btn-link text-3">Learn more<i class="fas fa-chevron-right text-1 ml-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

      


        </div>





        <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" style="width:143% ;display:none;">  <%--style="width:161%;"  class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto"--%>
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row"> <img src="Images/gallery/logo(ft).png" class="logo"> </div>
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="https://mobilerechargesoftware.co.in/wp-content/uploads/DMT-1.png" class="image"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                    <div class="row mb-4 px-3">
                        <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                        <div class="facebook text-center mr-3">
                            <div class="fa fa-facebook"></div>
                        </div>
                        <div class="twitter text-center mr-3">
                            <div class="fa fa-twitter"></div>
                        </div>
                        <div class="linkedin text-center mr-3">
                            <div class="fa fa-linkedin"></div>
                        </div>
                    </div>
                    <div class="row px-3 mb-4">
                        <div class="line"></div> <small class="or text-center">Or</small>
                        <div class="line"></div>
                    </div>
                   
                </div>
            </div>
        </div>
       
    </div>
</div>






        <div id="main-wrapper2" class="h-100" style="width:115%;display:none;" >
  <div class="container-fluid px-0 h-100">
    <div class="row no-gutters h-100">
    
      <div class="col-md-6">
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-primary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('New_Style/images/bg/image-3.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row no-gutters">
              <div class="col-10 col-lg-9 mx-auto">
                <div class="logo mt-5 mb-5 mb-md-0"> <a class="d-flex" href="home.aspx" title="fastgocash"><img src="Images/gallery/logo(ft).png" alt="fastgocash" style="width: 200px;"></a> </div>
              </div>
            </div>
              <div class="row no-gutters my-auto">
                <div class="col-10 col-lg-9 mx-auto">
                  <h1 class="text-11 text-white mb-4">Welcome back!</h1>
                  <p class="text-4 text-white line-height-4 mb-5">We are glad to see you again! Instant deposits, withdrawals &amp; payouts trusted by millions worldwide.</p>
                </div>
              </div>
          </div>
        </div>
      </div>
   

        


      <div class="col-md-6 d-flex align-items-center" ">
        <div class="container my-4">
          <div class="row">
            <div class="col-11 col-lg-9 col-xl-8 mx-auto">
              <h3 class="font-weight-400 mb-4">Log In</h3>
              <form id="loginForm2" method="post">
                <div class="form-group">
                  <label for="emailAddress">User Id</label>
                 <%-- <input type="email" class="form-control" id="emailAddress" required="" placeholder="Enter Your Email">--%>
                   <%-- <asp:TextBox runat="server" class="form-control" ID="UserName" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
							<%--<label class="">User Name</label>--%>
                </div>
                <div class="form-group">
                  <label for="loginPassword">Password</label>
                 <%-- <input type="password" class="form-control" id="loginPassword" required="" placeholder="Enter Password">--%>
                    <%--<asp:TextBox ID="Password" class="form-control"  TextMode="Password" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
                </div>
                <div class="row">
                  <div class="col-sm">
                    <div class="form-check custom-control custom-checkbox">
                      <input id="remember-me" name="remember" class="custom-control-input" type="checkbox">
                      <label class="custom-control-label" for="remember-me">Remember Me</label>
                    </div>
                  </div>
                  <div class="col-sm text-right"><a class="btn-link" href="#">Forgot Password</a></div>
                </div>
                <%--<button class="btn btn-primary btn-block my-4" type="submit">Login</button>--%>
                  <%--<asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-primary btn-block my-4"/>--%>
              </form>
              <p class="text-3 text-center text-muted">Don't have an account <a class="btn-link" href="Regs.aspx">Sign Up</a></p>
            </div>
          </div>
        </div>
      </div>
      <!-- Login Form End -->
    </div>
  </div>
</div>
        

         


        <div class="large-12 medium-12 small-12" style="display:none;">
            <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
            <div class="lft f16" style="display: none;">

                Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
            </div>
         
            <div class="clear1">
            </div>

            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                </div>
               
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="    font-size: 23px;" ></i></span>
                   
                </div>
         
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
       
                <br />

                <a href="../ForgotPassword.aspx" style="color:#ff0000">Forgot Password</a>
            </div>
            <div class="clear">
            </div>
            <div>
                <asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="clear">
            </div>
        </div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />
</asp:Login>



<script>
    var overlay = document.getElementById("overlay");

    // Buttons to 'switch' the page
    var openSignUpButton = document.getElementById("slide-left-button");
    var openSignInButton = document.getElementById("slide-right-button");

    // The sidebars
    var leftText = document.getElementById("sign-in");
    var rightText = document.getElementById("sign-up");

    // The forms
    var accountForm = document.getElementById("sign-in-info")
    var signinForm = document.getElementById("sign-up-info");

    // Open the Sign Up page
    openSignUp = () => {
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation-out");
        overlay.classList.remove("open-sign-in");
        rightText.classList.remove("overlay-text-right-animation");
        // Add classes for animations
        accountForm.className += " form-left-slide-out"
        rightText.className += " overlay-text-right-animation-out";
        overlay.className += " open-sign-up";
        leftText.className += " overlay-text-left-animation";
        // hide the sign up form once it is out of view
        setTimeout(function () {
            accountForm.classList.remove("form-left-slide-in");
            accountForm.style.display = "none";
            accountForm.classList.remove("form-left-slide-out");
        }, 700);
        // display the sign in form once the overlay begins moving right
        setTimeout(function () {
            signinForm.style.display = "flex";
            signinForm.classList += " form-right-slide-in";
        }, 200);
    }

    // Open the Sign In page
    openSignIn = () => {
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation");
        overlay.classList.remove("open-sign-up");
        rightText.classList.remove("overlay-text-right-animation-out");
        // Add classes for animations
        signinForm.classList += " form-right-slide-out";
        leftText.className += " overlay-text-left-animation-out";
        overlay.className += " open-sign-in";
        rightText.className += " overlay-text-right-animation";
        // hide the sign in form once it is out of view
        setTimeout(function () {
            signinForm.classList.remove("form-right-slide-in")
            signinForm.style.display = "none";
            signinForm.classList.remove("form-right-slide-out")
        }, 700);
        // display the sign up form once the overlay begins moving left
        setTimeout(function () {
            accountForm.style.display = "flex";
            accountForm.classList += " form-left-slide-in";
        }, 200);
    }

    // When a 'switch' button is pressed, switch page
    openSignUpButton.addEventListener("click", openSignUp, false);
    openSignInButton.addEventListener("click", openSignIn, false);
</script>
